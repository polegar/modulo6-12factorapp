Questão discursiva
---

**A) Explique o que acontece se o valor da variável de ambiente ENV_APP (atividade prática) for alterada.**
RESPOSTA:
Esta variável esta configurada no docker-compose.yml, portanto a alteração dela irá impactar diretamente na configuração da infraestrutura.

1. Irá alterar o volume que é atribuído ao container, permitindo alterar entre volumes de forma simplificada, e se for mudado para um valor inédito, será gerado um novo volume;
```bash
# Para verificar volumes
$ docker volume ls
```
2. Nos container api-rest e redis, irá alterar o metadado do container, adicionando um label diferente ao container gerado;
```bash
$ docker container ls --filter "label=app.environment=develop"
```
3. Irá alterar a tag configurar no driver de log, permitindo que os dados dos containers possam ser filtrados a partir do ambiente configurado na variável.

**B) Por que algumas configurações como ENV_APP, podem ficar como variáveis de ambiente e não dentro do docker-compose?**
RESPOTA:
As configurações ficando fora do docker-compose podem ser setadas em ferramentas de CI/CD, permitindo a proteção e automatização do ambiente.

