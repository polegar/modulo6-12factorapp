Atividades Práticas
---
## 1. Levantar a aplicação escalável

1. Faça o download do projeto no gitlab

```bash
$ apt-get update && apt-get install git 
$ mkdir modulo6 
$ cd modulo6
$ git clone https://gitlab.com/polegar/modulo6-12factorapp
$ cd modulo6-12factorapp/stages/fator-completo
```

2. Verifique o IP da sua máquina
```bash
$ ip -4 addr | grep "global dynamic" | awk '{print $2}'
```

3. Execute o servidor de logs

```bash
$ docker-compose -f graylog.yml up -d
```

4. Aguarde o servidor ser inicializado e acesse via url

```
http://localhost:9000
```

> Acesse com o usuário: admin e senha: admin

Uma vez acessada a interface, vamos criar um input para os dados que serão enviados via Docker

5. Va até o menu superior e clique em "Settings" > "Inputs"
6. Em "Select input", selecione GELF UDP e clique em "Launch new input"
7. Selecione a caixa "Global". insira um nome no campo "Title" e clique no botão salvar
8. Clique no botão "Show received messages"
9. De volta a pasta do projeto, podemos levantar nossa aplicação normalmente e escalar o projeto

```bash
$ ENV_APP=develop API_VERSION=0.0.1 GELF_HOST=seu_ip GELF_PORT=12201 docker-compose up -d --scale api-rest=1 --build
``` 
> Troque "seu_ip" pelo IP encontrado no passo 2.

> api-rest pode ser alterado do valor 1, para o número de replicas que você deseja da aplicação.

## 2. Fazer a aplicação conectar em um outro banco de dados redis e desativar o modo debug, sem fazer alteração no código fonte da aplicação

1. Modifique o arquivo docker-compose.yml, adicionando a variável de ambiente adequada

de
```yaml
...
services:
  api-rest:
    build: .
    image: 'api-rest:${API_VERSION}'
...
  redis:
    image: redis
...
```
para
```yaml
...
services:
  api-rest:
    build: .
    image: 'api-rest:${API_VERSION}'
    environment:
      - REDIST_HOST=meu_novo_redis
      - DEBUG=False
...
  meu_novo_redis:
    image: redis
...
```

> Lembre-se devido ao fator 3 (Configuração), a aplicação está configurada para coletar a informação de variável de ambiente REDIS_HOST

2. Rode novamente a aplicação, trocando a variável ENV_APP por um nome de sua escolha

```bash
$ ENV_APP=develop API_VERSION=0.0.1 GELF_HOST=seu_ip GELF_PORT=12201 docker-compose up -d --scale api-rest=1 --build
```

Carregue para plataforma um print dos logs com as informações sobre a aplicação que acabamos de provisionar.
